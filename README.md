# Custom Font 

Using XML: 

```xml
android:fontFamily="@font/cherry_cream_soda"
```

Pragmatically using Java

```java
textView = findViewById(R.id.defaultText);
textView.setTypeface(ResourcesCompat.getFont(this, R.font.gabriola));
```

https://stackoverflow.com/questions/27588965/how-to-use-custom-font-in-a-project-written-in-android-studio

### TESTED AND OK 

Farhan Sadik 

